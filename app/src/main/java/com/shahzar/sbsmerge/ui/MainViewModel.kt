package com.shahzar.sbsmerge.ui

import android.os.Environment
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arthenica.mobileffmpeg.FFmpeg
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class MainViewModel: ViewModel() {

    private var outputFile: String = "/storage/emulated/0/sbs-output.mp4"
    private var commandLineArgs = "-filter_complex hstack -c:v mpeg4"

    private val _videoPath1 = MutableLiveData<String>()
    private val _videoPath2 = MutableLiveData<String>()
    private val _outputPath = MutableLiveData<String>()
    private val _launchOutput = MutableLiveData<String>()
    private val _showProgress = LiveEvent<Boolean>()
    private val _onError = LiveEvent<String>()
    private val _onReset = LiveEvent<Boolean>()
    private val _onProcessCompleteSuccess = LiveEvent<String>()

    val videoPath1: LiveData<String>
        get() = _videoPath1

    val videoPath2: LiveData<String>
        get() = _videoPath2

    val outputPath: LiveData<String>
        get() = _outputPath

    val launchOutput: LiveData<String>
        get() = _launchOutput

    val showProgress: LiveEvent<Boolean>
        get() = _showProgress

    val onError: LiveEvent<String>
        get() = _onError

    val onReset: LiveEvent<Boolean>
        get() = _onReset

    val onProcessCompleteSuccess: LiveEvent<String>
        get() = _onProcessCompleteSuccess



    fun loadVideo(path: String, vidNum: Int) {
        when(vidNum) {
            1 -> _videoPath1.value = path
            2 -> _videoPath2.value = path
        }
        _onReset.value = true
    }


    fun mergeVideos() {

        // Setup Output File
        val datetime = SimpleDateFormat("dd-M-yyyy-hh-mm-ss").format(Calendar.getInstance().time)
        val sdPath = Environment.getExternalStorageDirectory().absolutePath
        outputFile = "$sdPath/sbs-output-$datetime.mp4"
        Log.d("sbs-custom-log", "Will log output to ${outputFile}")
        _outputPath.value = outputFile

        _showProgress.value = true

        GlobalScope.launch {

            // Calculate output resolution
            val size1 = getVideoResolution(videoPath1.value)
            val size2 = getVideoResolution(videoPath2.value)

            if (size1 != size2) {
                withContext(Dispatchers.Main) {
                    _onError.value = "Videos should have same resolution"
                    _showProgress.value = false
                }
                return@launch
            }

            commandLineArgs = commandLineArgs.plus(" -s $size1")

            FFmpeg.execute("-i '${videoPath1.value}' -i '${videoPath2.value}' $commandLineArgs $outputFile")

            withContext(Dispatchers.Main) {
                _showProgress.value = false

                val rc = FFmpeg.getLastReturnCode()

                when (rc) {
                    FFmpeg.RETURN_CODE_SUCCESS -> _onProcessCompleteSuccess.value = "Command execution completed successfully."
                    FFmpeg.RETURN_CODE_CANCEL -> _onError.value = "Command execution cancelled by user"
                    else -> {
                        _onError.value = String.format("Command execution failed with rc=%d and the output below.", rc)
                        FFmpeg.printLastCommandOutput(Log.INFO)
                    }
                }
            }
        }

    }

    fun launchOutput() {
        _launchOutput.value = outputFile
    }

    suspend fun getVideoResolution(inputFile: String?): String {
        val info = FFmpeg.getMediaInformation(inputFile)
        if (info.streams.size > 0) {
            val size = "${info.streams[0].width}x${info.streams[0].height}"
            return size
        }
        return  "720x480"
    }

}