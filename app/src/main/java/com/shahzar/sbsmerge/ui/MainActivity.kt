package com.shahzar.sbsmerge.ui

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.arthenica.mobileffmpeg.Config
import com.shahzar.sbsmerge.R
import com.shahzar.sbsmerge.util.PathUtil
import kotlinx.android.synthetic.main.activity_main.*
import com.arthenica.mobileffmpeg.FFmpeg
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_CANCEL
import com.arthenica.mobileffmpeg.FFmpeg.RETURN_CODE_SUCCESS
import android.os.Environment.getExternalStorageDirectory
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.progress_dialog_layout.view.*
import java.util.jar.Manifest


class MainActivity : AppCompatActivity() {

    var currentVidSelection: Int = 0
    var loader: AlertDialog? = null
    private lateinit var viewModel: MainViewModel

    companion object {
        private const val REQUEST_CODE_PICK_VIDEO = 44;
        private const val REQUEST_CODE_PERM = 46;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        init()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.add(R.string.merge)?.setOnMenuItemClickListener {
            viewModel.mergeVideos()
            updateUi()
            true
        }?.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
        return true
    }

    private fun init() {
        loader = createProgressDialog(getString(R.string.msg_processing))

        viewModel.videoPath1.observe(this, Observer {txt_video1_path.text = it})
        viewModel.videoPath2.observe(this, Observer {txt_video2_path.text = it})
        viewModel.outputPath.observe(this, Observer {output_path.text = it})
        viewModel.launchOutput.observe(this, Observer { launchVideo(it) })
        viewModel.showProgress.observe(this, Observer { showHideProgress(it) })
        viewModel.onProcessCompleteSuccess.observe(this, Observer { output_group.visibility = View.VISIBLE; showMsg(it, false) })
        viewModel.onError.observe(this, Observer { output_group.visibility = View.GONE; showMsg(it, true)})
        viewModel.onReset.observe(this, Observer { output_group.visibility = View.GONE })

        btn_video1.setOnClickListener { onVideoSelectClicked(1) }
        btn_video2.setOnClickListener { onVideoSelectClicked(2) }

        launch_output.setOnClickListener { viewModel.launchOutput() }

        handlePermission()
    }

    private fun updateUi() {
        Config.enableStatisticsCallback {
            runOnUiThread {
                status_frames.text = "Frames: ${it.videoFrameNumber}"
                status_time.text = "Time: ${it.time}"
            }
        }
    }

    private fun onVideoSelectClicked(vidNum: Int) {
        currentVidSelection = vidNum
        startActivityForResult(Intent(Intent.ACTION_GET_CONTENT)
            .setType("video/*")
            .putExtra(Intent.EXTRA_LOCAL_ONLY, true)
            .addCategory(Intent.CATEGORY_OPENABLE),
            REQUEST_CODE_PICK_VIDEO
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == REQUEST_CODE_PICK_VIDEO) {
            if (resultCode == Activity.RESULT_OK) {
                data?.data?.let {
                    loadVideo(it, currentVidSelection)
                }
            }
            return
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun loadVideo(uri: Uri, vidNum: Int) {
        viewModel.loadVideo(PathUtil.getPath(this, uri), vidNum)
    }

    private fun launchVideo(path: String) {

        val intent = Intent(Intent.ACTION_VIEW)
        val uri = Uri.parse(path)
        intent.setDataAndType(uri, "video/*")
        startActivity(intent)
    }

    private fun createProgressDialog(text: String): AlertDialog {
        val builder = AlertDialog.Builder(this)
        val view = LayoutInflater.from(this).inflate(R.layout.progress_dialog_layout, null)
        view.progressDialogText.text = text
        builder.setView(view)
        builder.setCancelable(false)
        return builder.create()
    }

    private fun handlePermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CODE_PERM)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE_PERM) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                showMsg("Permission required for application to work.", true)
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun showHideProgress(show: Boolean) {
        if(show) loader?.show() else loader?.hide()
    }

    private fun showMsg(msg: String, isError: Boolean) {
        val snackbar = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, if (isError) R.color.color_red else R.color.color_green))
        snackbar.show()
    }
}
